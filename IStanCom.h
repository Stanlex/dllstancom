#pragma once

#include<ObjBase.h>
#include<list>
#include<string>
// {16E977D0-1087-45DA-9D79-8DE857BDA669}
//Interface ID of the "Interface of StanCom"
static const GUID IID_IStanCom =
{ 0x16e977d0, 0x1087, 0x45da, { 0x9d, 0x79, 0x8d, 0xe8, 0x57, 0xbd, 0xa6, 0x69 } };

interface IStanCom :IUnknown // Stan's Interface. Expands IUnknown
{
	virtual HRESULT STDMETHODCALLTYPE SListServices(std::list<std::wstring>&) = 0;
	virtual HRESULT STDMETHODCALLTYPE SStopService(int num)=0;
	virtual HRESULT STDMETHODCALLTYPE SStartService(int num)=0;
	virtual HRESULT STDMETHODCALLTYPE SRestartService(int num)=0;
};