#pragma once
#include "IStanCom.h"

class StanCom : IStanCom //My Com object implements the IStanCom interface
{
protected:
	//Reference count will be needed
	long RefCount=0;
	SC_HANDLE SCManager; //Service manager called when manipulating WIndows services. Created on COM initialiyation. Disposed of when COM is unloaded.
	LPENUM_SERVICE_STATUS_PROCESSW services; //ListServices populates this. Can be used later to refer to particular services.
	DWORD ServicesCount; //The amount of services found. Each service should have an unchanging number between 0 and this.
	HRESULT EnumServices(SC_HANDLE SCManager, std::list<std::wstring>& Results);
	BOOL CheckManager();
	HRESULT STDMETHODCALLTYPE pControlService(size_t, BOOL); //Internal function generalizing SStart and SStop
	BOOL StopDependentServices(SC_HANDLE); //Internal function generalizing SStart and SStop
public:
	StanCom();
	~StanCom();
	virtual HRESULT STDMETHODCALLTYPE Init();

	//Required by IUnknown
	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppv);
	virtual ULONG   STDMETHODCALLTYPE AddRef(void); 
	virtual ULONG   STDMETHODCALLTYPE Release(void);

	//Required by IStanCom
	virtual HRESULT STDMETHODCALLTYPE SListServices(std::list<std::wstring>&);
	virtual HRESULT STDMETHODCALLTYPE SStopService(int num);
	virtual HRESULT STDMETHODCALLTYPE SStartService(int num);
	virtual HRESULT STDMETHODCALLTYPE SRestartService(int num);
};