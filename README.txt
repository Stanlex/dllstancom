This is my first foray in COM objects and Windows services. Bugs are to be expected and it's more of a proof-of-concept than a full fledged program.
This is the whole project, all source codes are present and most of my notes are present as code comments.
How to use:
	Compile/Use the compiled DLL/EXE files
	Register the library LStanCom.dll:
		cd (ProjectDirectory/Debug)
		regsvr32 LStanCom.dll
	Run ClientStanCom.exe as administrator.
		Select "List Services" first by typing "1" as prompted. (Confirm by hitting enter)
		Play with commands 2,3 and 4. List all services again if needed.
		Quit by typing in "0" or a non-numeric command.

Sources:
https://www.codeproject.com/articles/338268/com-in-c
https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-controlservice (And other articles)
https://docs.microsoft.com/en-us/windows/win32/services/stopping-a-service