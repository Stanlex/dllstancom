﻿// ClientStanCom.cpp : Tento soubor obsahuje funkci main. Provádění programu se tam zahajuje a ukončuje.
//

#include <iostream>
#include <ObjBase.h>
#include "./IStanCom.h" //Learn of the IStanCom interface and its GUID: IID_IStanCom
#include "./Registry.h" //Learn of the CLSID
#include <string>
#include <list>

//Testing imports

void PrintReturns(HRESULT ECode) {
    switch (ECode) {
    case E_BOUNDS:
        wprintf_s(L"Invalid service number.\n");
        break;
    case E_FAIL:
        wprintf_s(L"Service control failed. Error number: %d \n", GetLastError());
        break;
    case E_ACCESSDENIED:
        wprintf_s(L"Error getting Service Control manager. Error number: %d \n", GetLastError());
        wprintf_s(L"%ls \n", L"Do you have sufficient rights? (Run as admin)");
        break;
    case ERROR_FT_READ_FAILURE:
        wprintf_s(L"Error enumerating services. Error number: %d \n", GetLastError());
        break;
    case ERROR_TIMEOUT:
        wprintf_s(L"Service control timed out. \n");
        break;
    case E_NOT_VALID_STATE:
        wprintf_s(L"Query Service Status failed. Error number: (%d)\n", GetLastError());
        break;
    case E_ILLEGAL_STATE_CHANGE:
        wprintf_s(L"Illegal state change. (Stopping a stopped service. Starting a running one...) Doing nothing.\n");
        break;
    case S_OK:
        wprintf_s(L"Success!\n");
        break;
    default:
        wprintf_s(L"Unknown error! Error number: (%d)\n", GetLastError());
        break;
    }
}

void ProcessCommand(int Command, IStanCom* pCom) {
    int ServNum;
    std::list<std::wstring> Results;
    HRESULT ECode;
    switch (Command) {
    case 1:
        pCom->SListServices(Results);    
        for (std::list<std::wstring>::iterator it = Results.begin(); it != Results.end(); ++it) {
            wprintf_s(L"%ls", it->c_str());
        }
        break;
    case 2:
        wprintf_s(L"Which service should I stop? Give me a number.\n");
        std::cin >> ServNum;
        ECode = pCom->SStopService(ServNum);
        PrintReturns(ECode);
        break;
    case 3:
        wprintf_s(L"Which service should I start? Give me a number.\n");
        std::cin >> ServNum;
        ECode = pCom->SStartService(ServNum);
        PrintReturns(ECode);
        break;
    case 4:
        wprintf_s(L"Which service should I restart? Give me a number.\n");
        std::cin >> ServNum;
        ECode = pCom->SRestartService(ServNum);
        PrintReturns(ECode);
        break;
    default:
        wprintf_s(L"Unrecognized command. Please try again.\n");
        break;
    }
}

int main()
{
    CoInitialize(NULL);
    IStanCom* pCom = NULL;
    HRESULT hrA = CoCreateInstance(CLSID_StanCom, NULL, CLSCTX_INPROC_SERVER, IID_IStanCom, (void**)&pCom);
    if (SUCCEEDED(hrA)) {
        wprintf_s(L"Hello and welcome to my first foray to COM objects and windows services.\n");
        wprintf_s(L"Five commands are supported, repesented by numbers:\n");
        wprintf_s(L"0 - Shuts down.\n");
        wprintf_s(L"1 - Call this first. Lists all the services on your machine, gives you their identifiers and states.\n");
        wprintf_s(L"2 - Stops a service and its dependencies.\n");
        wprintf_s(L"3 - Starts a service.\n");
        wprintf_s(L"4 - Restarts a service.\n");
        int Reply;
        std::cin >> Reply;
        while (Reply!=0) {
            ProcessCommand(Reply, pCom);
            wprintf_s(L"Back to main menu!\n");
            //Load new 
            std::wcin >> Reply;
        }
        pCom->Release();
    }
    else {
        wprintf_s(L"Error loading the COM module...\n");
    }
    CoFreeUnusedLibraries();
}

// Spuštění programu: Ctrl+F5 nebo nabídka Ladit > Spustit bez ladění
// Ladění programu: F5 nebo nabídka Ladit > Spustit ladění

// Tipy pro zahájení práce:
//   1. K přidání nebo správě souborů použijte okno Průzkumník řešení.
//   2. Pro připojení ke správě zdrojového kódu použijte okno Team Explorer.
//   3. K zobrazení výstupu sestavení a dalších zpráv použijte okno Výstup.
//   4. K zobrazení chyb použijte okno Seznam chyb.
//   5. Pokud chcete vytvořit nové soubory kódu, přejděte na Projekt > Přidat novou položku. Pokud chcete přidat do projektu existující soubory kódu, přejděte na Projekt > Přidat existující položku.
//   6. Pokud budete chtít v budoucnu znovu otevřít tento projekt, přejděte na Soubor > Otevřít > Projekt a vyberte příslušný soubor .sln.
