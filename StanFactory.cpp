#include "pch.h"
#include "StanFactory.h"
#include "StanCom.h"


StanFactory::StanFactory(void){
	FactRefCount = 1;
}


StanFactory::~StanFactory(void) {
}


//IUnknown
HRESULT STDMETHODCALLTYPE StanFactory::QueryInterface(REFIID riid, void** ppv) {
	HRESULT res = S_OK;
	//Comments in StanCom.cpp
	if (riid == IID_IUnknown) { *ppv = (IUnknown*)this; }
	else if (riid == IID_IClassFactory) { *ppv = (IClassFactory*)this;}
	else {res = E_NOINTERFACE;}

	if (res == S_OK) { AddRef(); }
	return res;
}

ULONG   STDMETHODCALLTYPE StanFactory::AddRef(void) {
	InterlockedIncrement(&FactRefCount);
	return FactRefCount;
}

ULONG   STDMETHODCALLTYPE StanFactory::Release(void) {
	InterlockedDecrement(&FactRefCount);

	if (FactRefCount == 0) {
		delete this;
		return 0;
	}
	else return FactRefCount;
}

HRESULT STDMETHODCALLTYPE StanFactory::CreateInstance(LPUNKNOWN pUnk, const IID& id, void** ppv) {
	HRESULT res = E_UNEXPECTED;
	if (pUnk != NULL) res = CLASS_E_NOAGGREGATION; // TODO: Why this?
	else if (id == IID_IStanCom || id == IID_IUnknown)
	{
		StanCom* pA = new StanCom();
		if (pA == NULL) { res = E_OUTOFMEMORY; }
		else { res = pA->Init(); }

		if (FAILED(res)) {
			// initialization failed, delete component
			pA->Release();
			return res;
		}
		res = pA->QueryInterface(id, ppv);
		pA->Release(); //We don't need the whole COM object anymore. Just the interface we asked for.
		return res;
	}
	return res;
}

// Add(true) or remove(false) a lock. Required but rarely used.
HRESULT STDMETHODCALLTYPE StanFactory::LockServer(BOOL fLock) {
	if (fLock)  InterlockedIncrement(&g_ComLocks);
	else    InterlockedDecrement(&g_ComLocks);
	return S_OK;
}