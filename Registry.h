#pragma once
#include <ObjBase.h>


// {F560CE1D-07C4-4F53-88DA-CE3D850007A1}. The ID of our COM object
GUID CLSID_StanCom = { 0xf560ce1d, 0x7c4, 0x4f53, { 0x88, 0xda, 0xce, 0x3d, 0x85, 0x0, 0x7, 0xa1 } };

HMODULE hStanCom; //Handle to our StanCom module. Filled when the DLL loads.

const WCHAR FName[] = L"Stan Com";
const WCHAR VerInd[] = L"Stans.Component.1"; //Version Indicator
const WCHAR ProgId[] = L"Stans.Component";

STDAPI DllInstall(char* s);
STDAPI DllRegisterServer();
STDAPI DllUnregisterServer();
// This function will register a component in the Registry.
// The component calls this function from its DllRegisterServer function.
HRESULT RegisterServer(HMODULE hModule,
    const CLSID& clsid,
    const WCHAR* szFriendlyName,
    const WCHAR* szVerIndProgID,
    const WCHAR* szProgID);

// This function will unregister a component.  Components
// call this function from their DllUnregisterServer function.
HRESULT UnregisterServer(const CLSID& clsid,
    const WCHAR* szVerIndProgID,
    const WCHAR* szProgID);