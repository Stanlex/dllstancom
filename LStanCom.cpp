// LStanCom.cpp : Defines the exported functions for the DLL application. All else is internal
//

#include "StanFactory.h"
#include "StanCom.h"

ULONG g_ComLocks; //Starts at 0 implicit.
extern  GUID CLSID_StanCom;

STDAPI DllCanUnloadNow() {
	HRESULT res = E_UNEXPECTED;
	if (g_ComLocks == 0) { res = S_OK; } else { res = S_FALSE; }
	return res;
};
STDAPI DllGetClassObject(const CLSID& clsid, const IID& iid, void** ppv) {
	HRESULT res = E_UNEXPECTED;

	if (clsid == CLSID_StanCom) {
		//Make a standard factory. Have it create one object and discard the factory
		StanFactory* SFac = new StanFactory();
		res = SFac->QueryInterface(iid, ppv);
		SFac->Release();
	}
	else
		res = CLASS_E_CLASSNOTAVAILABLE;

	return res;
};