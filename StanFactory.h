#pragma once
#include <ObjBase.h>

//How many factories (regardless of type) are there?
//Is declared at library level. Can be seen globally
extern ULONG g_ComLocks;     


class StanFactory :
	public IClassFactory
{
protected:
	// Reference count
	long FactRefCount;

public:
	StanFactory(void);
	~StanFactory(void);

	//IUnknown
	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID riid, void** ppv);
	virtual ULONG   STDMETHODCALLTYPE AddRef(void);
	virtual ULONG   STDMETHODCALLTYPE Release(void);

	/*IClassFactory implementation:*/
	virtual HRESULT STDMETHODCALLTYPE CreateInstance(LPUNKNOWN pUnk, const IID& id, void** ppv); //creates COM instance
	virtual HRESULT STDMETHODCALLTYPE LockServer(BOOL fLock); //provides server locking. Required
};