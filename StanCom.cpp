#include "pch.h"
#include "StanCom.h"
#include <string>
#include <iostream>
#include <list>

StanCom::StanCom() {
    services = NULL;
    ServicesCount = 0;
	RefCount = 1;
	SCManager = OpenSCManagerW(NULL, NULL, SC_MANAGER_ALL_ACCESS);
	//Stuff to do when the object is created.
}
StanCom::~StanCom() {
    if (services != NULL) { //ListServices has been called. Free.
        free(services);
    }
	CloseServiceHandle(SCManager);
	//Stuff to do when the object is destroyed
}
HRESULT STDMETHODCALLTYPE StanCom::QueryInterface(REFIID riid, void** ppv) {
	//ppv the value of a pointer to (pointer to value).
	// Treat this as a pointer passed by reference. 
	// We want the pointer to point to our COM object.
	// iff the object implements the REFIID interface.
	HRESULT res = S_OK;
	if (riid == IID_IUnknown) { *ppv = (IUnknown*)this; } // Query: Do you implement IUnknown? -> Yes. I am a COM
	else if (riid == IID_IStanCom) { *ppv = (IStanCom*)this; } //Query: Do you implement IStanCom? -> Yes, I do.
	// If the riid is something we have no idea how to implement, return an error.
	else { res = E_NOINTERFACE; }	
	//If we actually return a valid pointer, one more client knows about us.
	if (res == S_OK) { AddRef(); }

	return res;
}
ULONG StanCom::AddRef() {
	//Atomic operation. Threadsafe incrementing. Requires a pointer to the LONG value
	//Or an explicitly VOLATILE variable (?TODO What is that for?)
	InterlockedIncrement(&RefCount);
	return RefCount;
}
ULONG StanCom::Release() {
	InterlockedDecrement(&RefCount);
	if (RefCount == 0) {
		//No-one knows us anymore. We disappear
		delete this;
		return 0;
	}
	return RefCount;
}

BOOL StanCom::CheckManager() {
    if (!SCManager){return FALSE;}
    return TRUE;
}


HRESULT StanCom::EnumServices(SC_HANDLE SCManager, std::list<std::wstring>& Results)
{
    //CZECH SPECIFIC!!! Get local? TODO
    setlocale(LC_ALL, "cs-CZ");

    static const WCHAR runningW[] = { ' ',' ',' ',' ','%','s','\n',0 };
    DWORD size, i, resume;
    if (!CheckManager()) { return E_ACCESSDENIED; }

    EnumServicesStatusExW(SCManager, SC_ENUM_PROCESS_INFO, SERVICE_WIN32, SERVICE_STATE_ALL, NULL, 0, &size, &ServicesCount, NULL, NULL);
    if (GetLastError() != ERROR_MORE_DATA)
    {
        CloseServiceHandle(SCManager);
        return ERROR_FT_READ_FAILURE;
    }
    if (services != NULL) {
        free(services);
    }
    services = (LPENUM_SERVICE_STATUS_PROCESSW)malloc(size);
    resume = 0;
    if (!EnumServicesStatusExW(SCManager, SC_ENUM_PROCESS_INFO, SERVICE_WIN32, SERVICE_STATE_ALL, (LPBYTE)services, size, &size, &ServicesCount, &resume, NULL))
    {
        CloseServiceHandle(SCManager);
        return ERROR_FT_READ_FAILURE;
    }
    wchar_t Status[15];
    Results.clear();
    for (i = 0; i < ServicesCount; i++)
    {
        switch (services[i].ServiceStatusProcess.dwCurrentState) {
        case SERVICE_PAUSED:
            wcscpy_s(Status, L"PAUSED");
            break;
        case SERVICE_RUNNING:
            wcscpy_s(Status, L"RUNNING");
            break;
        case SERVICE_STOPPED:
            wcscpy_s(Status, L"STOPPED");
            break;
        case SERVICE_CONTINUE_PENDING:
            wcscpy_s(Status, L"PENDING PAUSE");
            break;
        case SERVICE_PAUSE_PENDING:
            wcscpy_s(Status, L"PENDING PAUSE");
            break;
        case SERVICE_START_PENDING:
            wcscpy_s(Status, L"PENDING START");
            break;
        case SERVICE_STOP_PENDING:
            wcscpy_s(Status, L"PENDING STOP");
            break;
        default:
            wcscpy_s(Status, L"UNKNOWN");
            break;
        }
        Results.emplace_back(L"Num: " + std::to_wstring(i) + L", " + Status + L" , " + services[i].lpDisplayName + L" \n");
    }
    return S_OK;
}

HRESULT STDMETHODCALLTYPE StanCom::SListServices(std::list<std::wstring> &Results) {
    return EnumServices(SCManager, Results);
}
//Source: https://docs.microsoft.com/en-us/windows/win32/services/stopping-a-service , Customization minimal
BOOL StanCom::StopDependentServices(SC_HANDLE Handle) {
    DWORD i;
    DWORD dwBytesNeeded;
    DWORD dwCount;

    LPENUM_SERVICE_STATUS   lpDependencies = NULL;
    ENUM_SERVICE_STATUS     ess;
    SC_HANDLE               hDepService;
    SERVICE_STATUS_PROCESS  ssp;

    DWORD dwStartTime = GetTickCount();
    DWORD dwTimeout = 30000; // 30-second time-out

    // Pass a zero-length buffer to get the required buffer size.
    if (EnumDependentServices(Handle, SERVICE_ACTIVE,
        lpDependencies, 0, &dwBytesNeeded, &dwCount))
    {
        // If the Enum call succeeds, then there are no dependent
        // services, so do nothing.
        return TRUE;
    }
    else
    {
        if (GetLastError() != ERROR_MORE_DATA)
            return FALSE; // Unexpected error

        // Allocate a buffer for the dependencies.
        lpDependencies = (LPENUM_SERVICE_STATUS)HeapAlloc(
            GetProcessHeap(), HEAP_ZERO_MEMORY, dwBytesNeeded);

        if (!lpDependencies) //Allocation failed.
            return FALSE;

        __try {
            // Enumerate the dependencies.
            if (!EnumDependentServices(Handle, SERVICE_ACTIVE,
                lpDependencies, dwBytesNeeded, &dwBytesNeeded,
                &dwCount))
                return FALSE;

            for (i = 0; i < dwCount; i++)
            {
                ess = *(lpDependencies + i);
                // Open the service.
                hDepService = OpenService(SCManager,
                    ess.lpServiceName,
                    SERVICE_STOP | SERVICE_QUERY_STATUS);

                if (!hDepService)
                    return FALSE;

                __try {
                    // Send a stop code.
                    if (!ControlService(hDepService,
                        SERVICE_CONTROL_STOP,
                        (LPSERVICE_STATUS)&ssp))
                        return FALSE;

                    // Wait for the service to stop.
                    while (ssp.dwCurrentState != SERVICE_STOPPED)
                    {
                        Sleep(ssp.dwWaitHint);
                        if (!QueryServiceStatusEx(
                            hDepService,
                            SC_STATUS_PROCESS_INFO,
                            (LPBYTE)&ssp,
                            sizeof(SERVICE_STATUS_PROCESS),
                            &dwBytesNeeded))
                            return FALSE;

                        if (ssp.dwCurrentState == SERVICE_STOPPED)
                            break;

                        if (GetTickCount() - dwStartTime > dwTimeout)
                            return FALSE;
                    }
                }
                __finally
                {
                    // Always release the service handle.
                    CloseServiceHandle(hDepService);
                }
            }
        }
        __finally
        {
            // Always free the enumeration buffer.
            HeapFree(GetProcessHeap(), 0, lpDependencies);
        }
    }
    return TRUE;
}

//Start and Stop service in one
#define MySSP services[ServNumber].ServiceStatusProcess //Called so often in this i should just get a macro.
HRESULT STDMETHODCALLTYPE StanCom::pControlService(size_t ServNumber, BOOL TurnON) {
    //Sanity check
    if (ServNumber<0 || ServNumber>=ServicesCount) {
        // If we have 5 services, valid numbers are 0 to 4
        return E_BOUNDS;
    }
    DWORD dwBytesNeeded;
    DWORD dwStartTime = GetTickCount(); //For Timeout reasons we need to know when we started.
    DWORD dwTimeOut = 30000;
    if (!CheckManager()) { return E_ACCESSDENIED; }
    SC_HANDLE MyServHandle = OpenServiceW(SCManager, services[ServNumber].lpServiceName, SC_MANAGER_ALL_ACCESS);
    if (MyServHandle == NULL) {
        wprintf_s(L"Open service failed (%d) \n", GetLastError());
        CloseServiceHandle(MyServHandle);
        return E_FAIL;
    }
    if (TurnON) {
        if (MySSP.dwCurrentState != SERVICE_STOPPED) {
            CloseServiceHandle(MyServHandle);
            return E_ILLEGAL_STATE_CHANGE;
        }
    }
    else {
        if (MySSP.dwCurrentState != SERVICE_RUNNING) {
            CloseServiceHandle(MyServHandle);
            return E_ILLEGAL_STATE_CHANGE;
        }
    }

    //Maybe TODO. Could do checks for all other states if needed and do custon errors for each. This catches everything though.
    
    //Suppose the service is running. Are there dependencies?
    // When turning ON, the StartService does this for us automatically. 
    if (!TurnON) {
        if (!StopDependentServices(MyServHandle)) {
            CloseServiceHandle(MyServHandle);
            return E_FAIL;
        };
    }
    //Send the STOP/START signal
    if (TurnON) {
        if (!StartService(MyServHandle, NULL,NULL))
        {
            CloseServiceHandle(MyServHandle);
            return E_FAIL;
        }
    }else{
        if (!ControlService(MyServHandle, SERVICE_CONTROL_STOP, (LPSERVICE_STATUS)&services[ServNumber].ServiceStatusProcess))
        {
            CloseServiceHandle(MyServHandle);
            return E_FAIL;
        }
    }

    // Wait for the service to finish changing state.
    DWORD RequiredState;
    if (TurnON) { RequiredState = SERVICE_RUNNING; }
    else { RequiredState = SERVICE_STOPPED; }
    while (MySSP.dwCurrentState != RequiredState)
    {
        Sleep(services[ServNumber].ServiceStatusProcess.dwWaitHint);
        if (!QueryServiceStatusEx(MyServHandle, SC_STATUS_PROCESS_INFO, (LPBYTE)&MySSP, sizeof(SERVICE_STATUS_PROCESS), &dwBytesNeeded))
        {
            CloseServiceHandle(MyServHandle);
            return E_NOT_VALID_STATE;
        }

        if (MySSP.dwCurrentState == SERVICE_STOPPED)
            break;

        if (GetTickCount() - dwStartTime > dwTimeOut)
        {
            CloseServiceHandle(MyServHandle);
            return ERROR_TIMEOUT;
        }
    }
    CloseServiceHandle(MyServHandle);
    return S_OK;
}

HRESULT STDMETHODCALLTYPE StanCom::SStartService(int ServNumber) {
    return pControlService(ServNumber, TRUE);
}
//Basically copypasted and a little customized from https://docs.microsoft.com/en-us/windows/win32/services/stopping-a-service
HRESULT STDMETHODCALLTYPE StanCom::SStopService(int ServNumber) {
    return pControlService(ServNumber, FALSE);
}
HRESULT STDMETHODCALLTYPE StanCom::SRestartService(int num) {
    //Turn it OFF;
    if (pControlService(num, FALSE) == E_FAIL) { return E_FAIL; }
    //Turn it ON
    if (pControlService(num, TRUE) == E_FAIL) { return E_FAIL; }
    return S_OK;
}

HRESULT STDMETHODCALLTYPE StanCom::Init() {
	// Just in case. Constructor should handle all the work.
	return S_OK;
}
